# Väder i Solf


## Description
Displays the current temperature in Solf. Both outside and inside temperatures are shown. The URL to the server is redacted so it is unfortunately not possible to use this app without supplying your own server. The backend should return a .json file when receiving a GET request. The JSON file should contain the outside temperature as the first value and the inside temperature as the second value.

### Update in version 1.1
The current wind power generation in Finland can now also be shown, both in the app and on the cover. To use this you need an API key from fingrid that you can get here https://data.fingrid.fi/en/pages/apis.

### Update in version 1.2
You can now view the current spot price for the electricity grid in Finland. The app can now also auto update its values at set intervals. For spot prices you need an API key from https://transparency.entsoe.eu/.


## Visuals
<img src="images/App_open.png" alt="First page for the app" width="300"/>
<img src="images/Cover.png" alt="The cover" width="300"/>
<img src="images/Settings.png" alt="The settings page" width="300"/>
