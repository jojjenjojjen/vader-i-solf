import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import QtQuick.XmlListModel 2.0
Page {
    id: page
    objectName: "FirstPage"
    property string outsideTemp: outside.text
    property string insideTemp: inside.text
    property string spotPrice: price.text
    property int windValue: wind.value
    property int windMaxValue: wind.maximumValue
    property bool tempsLoaded: false
    property string today: getTodayDate();
    property string tomorrow: getTomorrowDate();


    ConfigurationGroup {
        id: settings
        path: "/org/vader/solf/settings"
        property double refreshTimeInMinutes: 1
        property double transferFee: 0
        property double electricityTax: 0
        property int decimals: 1
        property int autoRefresh: 0
        property int graphHeight: 450
        property bool loadWindData: true
        property bool loadSpotPrice: true
        property bool showOriginalPrice: true
        property bool showPriceGraph: true

    }


    onStatusChanged: {
            if (status === PageStatus.Activating) {
                if (!tempsLoaded) {
                    refresh();
                    tempsLoaded = true
                }
                if (settings.autoRefresh != 0 && !refresh_timer.running)
                    refresh_timer.start()
            }
        }

    onActiveFocusChanged: {
        price_canvas.requestPaint()
    }

    Timer {
        id: refresh_timer
        interval: 1000 * 60 * settings.refreshTimeInMinutes
        repeat: true
        triggeredOnStart: false
        onTriggered: {
            refresh()
        }
    }
    Timer {
        id: refresh_max_wind_timer
        interval: 2000
        repeat: false
        triggeredOnStart: false
        onTriggered: maxWindRequest()
    }

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // The model that fetches and saves the spot price for the whole day
    XmlListModel {
        id: electricityPriceXML
        source: "https://web-api.tp.entsoe.eu/api?securityToken=[REDACTED]&documentType=A44&In_Domain=10YFI-1--------U&Out_Domain=10YFI-1--------U&periodStart="+today+"0000&periodEnd="+tomorrow+"0000"
        query: "/Publication_MarketDocument/TimeSeries/Period/Point"
        namespaceDeclarations: "declare default element namespace 'urn:iec62325.351:tc57wg16:451-3:publicationdocument:7:3';"

        XmlRole { name: "price"; query: "price.amount/number()" }

        onStatusChanged: {
            if(status === XmlListModel.Ready) {
                var hour = getHour()
                price.text = Math.round((electricityPriceXML.get(hour - 1).price / 10 + settings.transferFee) * ((settings.electricityTax + 100) / 100)
                                        * Math.pow(10,settings.decimals)) / Math.pow(10,settings.decimals)  + "c / kWh"
                if (settings.showOriginalPrice && (settings.transferFee != 0 || settings.electricityTax != 0)) {
                    price.text += " <font size='1'>(" + Math.round((electricityPriceXML.get(hour - 1).price / 10)
                                        * Math.pow(10,settings.decimals)) / Math.pow(10,settings.decimals)  + "c / kWh)</font>"
                }
                price_canvas.requestPaint()
            } else if (status === XmlListModel.Loading) {
                price.text = "..."
            }
       }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        VerticalScrollDecorator {}

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("Inställningar")
                onClicked: pageStack.animatorPush(Qt.resolvedUrl("SecondPage.qml"))
            }
            MenuItem {
                text: qsTr("Uppdatera")
                onClicked: refresh()
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height + Theme.paddingLarge

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Väder i Solf")
            }

            Label {
                x: Theme.horizontalPageMargin
                text: qsTr("Inomhus")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                id: inside
                x: Theme.horizontalPageMargin
                text: qsTr("...")
                font.pixelSize: Theme.fontSizeLarge
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Label {
                x: Theme.horizontalPageMargin
                text: qsTr("Utomhus")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                id: outside
                x: Theme.horizontalPageMargin
                text: "..."
                font.pixelSize: Theme.fontSizeLarge
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                x: Theme.horizontalPageMargin
                text: qsTr("Elpris")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
                anchors.horizontalCenter: parent.horizontalCenter
                visible: settings.loadSpotPrice
            }
            Label {
                id: price
                x: Theme.horizontalPageMargin
                text: "..."
                font.pixelSize: Theme.fontSizeLarge
                anchors.horizontalCenter: parent.horizontalCenter
                visible: settings.loadSpotPrice
            }
            Rectangle {
                width: parent.width - 4 * Theme.paddingLarge
                height: settings.graphHeight
                anchors {
                    bottomMargin: Theme.paddingLarge
                    horizontalCenter: parent.horizontalCenter
                }
                border.color: Theme.secondaryHighlightColor
                color: "transparent"
                visible: settings.showPriceGraph

                // create values for x-axis
                Repeater {
                    model: 22
                    delegate: Label {
                        color: Theme.primaryColor
                        font.pixelSize: Theme.fontSizeLarge / 2
                        text: (index%2==1) ? index + 1:""
                        anchors {
                            top: parent.bottom
                            topMargin: Theme.paddingSmall
                            left: parent.left
                            leftMargin: parent.width / 23 * index
                        }
                    }
                }

                // create values for y-axis
                Repeater {
                    model: (electricityPriceXML.status == XmlListModel.Ready? 5:0)
                    delegate: Label {
                        color: Theme.primaryColor
                        font.pixelSize: Theme.fontSizeLarge / 2
                        text: Math.round(((get_max_daily_price() - get_min_daily_price()) / 5 * index + get_min_daily_price())* Math.pow(10,settings.decimals)) / Math.pow(10,settings.decimals)
                        anchors {
                            bottom: parent.bottom
                            bottomMargin: parent.height / 5 * index
                            right: parent.left
                            rightMargin: Theme.paddingSmall
                        }
                    }

                }

                Label {
                    color: Theme.primaryColor
                    font.pixelSize: Theme.fontSizeLarge / 2
                    text: "c/kWh"
                    anchors {
                        top: parent.top
                        left: parent.left
                        leftMargin: Theme.paddingSmall
                    }
                }

                Canvas {
                    id: price_canvas
                    anchors.fill: parent
                    onPaint: {
                        var ctx = price_canvas.getContext("2d");
                        // draw horizontal grid lines
                        ctx.save;
                        ctx.strokeStyle = Theme.secondaryHighlightColor;
                        ctx.lineWidth = 3;
                        ctx.globalAlpha = 0.4;
                        for (var j=height/5; j<height; j+=height/5) {
                            ctx.beginPath();
                            ctx.moveTo(0,j);
                            ctx.lineTo(width,j);
                            ctx.stroke();
                        }
                        ctx.restore();

                        // draw price line
                        ctx.save();
                        ctx.strokeStyle = Theme.highlightColor;
                        ctx.lineWidth = 3;
                        ctx.globalAlpha = 1;
                        ctx.beginPath();
                        ctx.moveTo(0,height);
                        if (electricityPriceXML.status == XmlListModel.Ready) {
                            var highest_price = get_max_daily_price();
                            var lowest_price = get_min_daily_price();
                            for (var k=0; k<24; k++) {
                                ctx.lineTo(width/23*k, height - (electricityPriceXML.get(k).price/10 - lowest_price) /(highest_price-lowest_price) * height);
                                ctx.lineTo(width/23*(k+1), height - (electricityPriceXML.get(k).price/10 - lowest_price) /(highest_price-lowest_price) * height);
                            }
                        }
                        ctx.stroke();
                        ctx.restore();

                        // draw zero line when price falls under zero
                        if (electricityPriceXML.status == XmlListModel.Ready && lowest_price < 0 && highest_price > 0) {
                            ctx.save();
                            ctx.strokeStyle = Theme.HighlightColor;
                            ctx.lineWidth = 2;
                            ctx.globalAlpha = 0.8;
                            ctx.beginPath();
                            ctx.moveTo(0, height * highest_price / (highest_price-lowest_price));
                            ctx.lineTo(width, height * highest_price / (highest_price-lowest_price));
                            ctx.stroke();
                            ctx.restore();
                        }

                        // mark the current hour
                        if (electricityPriceXML.status == XmlListModel.Ready) {
                            var current_hour = getHour();
                            ctx.save();
                            ctx.strokeStyle = Theme.secondaryHighlightColor;
                            ctx.lineWidth = width/23 - 3;
                            ctx.globalAlpha = 0.4;
                            ctx.beginPath();
                            ctx.moveTo(width/23 * (current_hour-0.5), height);
                            ctx.lineTo(width/23 * (current_hour-0.5), 0);
                            ctx.stroke();
                            ctx.restore();
                        }
                    }
                }
                // place a 0 on the y-axis when zero line is visible
                /*Label {
                    color: Theme.primaryColor
                    font.pixelSize: Theme.fontSizeLarge / 2
                    visible: electricityPriceXML.status == XmlListModel.Ready && get_min_daily_price() < 0 && get_max_daily_price() > 0
                    text: "0"
                    anchors {
                        bottom: parent.bottom
                        bottomMargin: parent.height * get_max_daily_price() / (get_max_daily_price()-get_min_daily_price())
                        right: parent.left
                        rightMargin: Theme.paddingSmall
                    }
                }*/
            }
            Label {
                x: Theme.horizontalPageMargin
                text: qsTr("Vindel")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
                anchors.horizontalCenter: parent.horizontalCenter
                visible: settings.loadWindData
            }
            Slider {
                id: wind
                width: parent.width
                enabled: false
                handleVisible: false
                label: value + " MW / " + maximumValue + "MW"
                visible: settings.loadWindData
            }
        }
    }
    Label {
        x: Theme.horizontalPageMargin
        text: qsTr("Autouppdatering på")
        color:Theme.secondaryColor
        font.pixelSize:  Theme.fontSizeExtraSmall
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.margins: 10
        visible: settings.autoRefresh != 0
    }


    //## FUNCTIONS ##

    function refresh() {
        // Refresh all values that are enabled
        outside.text = "..."
        inside.text = "..."
        tempRequest([REDACTED])
        if (settings.loadWindData) {
            wind.value = 0
            windRequest()
        }
        if (tempsLoaded && settings.loadSpotPrice) { // Don't reload spot price on first refresh as it is automatically fetched the first time
            electricityPriceXML.reload()
        }
    }

    function tempRequest(url) {
        var xmlhttp = new XMLHttpRequest()
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE
                    && xmlhttp.status == 200) {
                outside.text = Math.round(JSON.parse(xmlhttp.responseText)[0].value * Math.pow(10,settings.decimals)) / Math.pow(10,settings.decimals) + "°C"
                inside.text = Math.round(JSON.parse(xmlhttp.responseText)[1].value * Math.pow(10,settings.decimals)) / Math.pow(10,settings.decimals) + "°C"
            }
        }
        xmlhttp.open("GET", url, true)
        xmlhttp.send()
    }

    function windRequest() {
        // Get current wind power generation
        var xmlhttp = new XMLHttpRequest()
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE
                    && xmlhttp.status == 200) {
                wind.value = JSON.parse(xmlhttp.responseText).value
                refresh_max_wind_timer.start()
            }
        }
        xmlhttp.open("GET", "https://data.fingrid.fi/api/datasets/181/data/latest")
        xmlhttp.setRequestHeader( 'x-api-key', [REDACTED])
        xmlhttp.send()
    }

    function maxWindRequest() {
        // Get maximum possible wind power generation
        var xmlhttp2 = new XMLHttpRequest()
        xmlhttp2.onreadystatechange = function () {
            if (xmlhttp2.readyState === XMLHttpRequest.DONE
                    && xmlhttp2.status == 200) {
                wind.maximumValue = JSON.parse(xmlhttp2.responseText).value
            }
        }
        xmlhttp2.open("GET", "https://data.fingrid.fi/api/datasets/268/data/latest")
        xmlhttp2.setRequestHeader( 'x-api-key', [REDACTED])
        xmlhttp2.send()
    }

    function getTodayDate() {
        var today = new Date();
        var todayDD = today.getDate().toString();
        var todayMM = (today.getMonth() + 1).toString();

        if (todayDD.length === 1)
            todayDD = 0 + todayDD;
        if (todayMM.length === 1)
            todayMM = 0 + todayMM;
        today = today.getFullYear().toString() + todayMM + todayDD;
        return today
    }

    function getTomorrowDate() {
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);

        var tomorrowDD = tomorrow.getDate().toString();
        var tomorrowMM = (tomorrow.getMonth() + 1).toString();
        if (tomorrowDD.length === 1)
            tomorrowDD = 0 + tomorrowDD;
        if (tomorrowMM.length === 1)
            tomorrowMM = 0 + tomorrowMM;
        tomorrow = tomorrow.getFullYear().toString() + tomorrowMM + tomorrowDD;
        return tomorrow
    }

    function getHour() {
        var time = new Date()
        return time.getHours()
    }

    function update_timer(index) {
        if (index !== 0) {
            refresh_timer.start()
        }
        else {
            refresh_timer.stop()
        }

    }

    function get_max_daily_price() {
        var maximum_value = -1000;
        for (var i = 0; i < 23; i++) {
            var i_price = electricityPriceXML.get(i).price / 10;
            if (i_price > maximum_value) {
                maximum_value = i_price;
            }
        }
        return maximum_value;
    }

    function get_min_daily_price() {
        var minimum_value = 1000;
        for (var i = 0; i < 23; i++) {
            var i_price = electricityPriceXML.get(i).price / 10;
            if (i_price < minimum_value) {
                minimum_value = i_price;
            }
        }
        return minimum_value;
    }
}
