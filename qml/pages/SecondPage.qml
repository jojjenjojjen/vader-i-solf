import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    ConfigurationGroup {
        id: settings
        path: "/org/vader/solf/settings"
        property double refreshTimeInMinutes: 1
        property double transferFee: 0
        property double electricityTax: 0
        property int decimals: 1
        property int autoRefresh: 0
        property int graphHeight: 450
        property bool loadWindData: true
        property bool loadSpotPrice: true
        property bool showWindOnCover: false
        property bool showPriceOnCover: false
        property bool showOriginalPrice: true
        property bool showPriceGraph: true
    }


    onStatusChanged: {
        if (status === PageStatus.Deactivating) {
                pageStack.find(function(page) { return page.objectName === "FirstPage"; }).refresh()
            }
        }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge
        VerticalScrollDecorator {}

        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Inställningar")
            }


            SectionHeader {
                text: qsTr("Generellt")
            }
            Slider {
                id: decimals
                width: parent.width
                minimumValue: 0
                maximumValue: 4
                value: settings.decimals
                stepSize: 1
                valueText: value
                label: qsTr("Antal decimaler")
                onReleased: {
                    settings.decimals = value
                }
            }


            SectionHeader {
                text: qsTr("Elpris")
            }
            TextSwitch {
                checked: settings.loadSpotPrice
                text: qsTr("Visa nuvarande elpris")
                onCheckedChanged: {settings.loadSpotPrice = checked}
            }
            TextSwitch {
                enabled: settings.loadSpotPrice && !settings.showWindOnCover
                checked: settings.showPriceOnCover
                text: qsTr("Visa nuvarade elpris på coverbilden")
                onCheckedChanged: {settings.showPriceOnCover = checked}
            }
            TextSwitch {
                enabled: settings.loadSpotPrice
                checked: settings.showOriginalPrice
                text: qsTr("Visa elpriset utan överföringsavgift och elskatt inom parentes")
                onCheckedChanged: {settings.showOriginalPrice = checked}
            }
            TextSwitch {
                enabled: settings.loadSpotPrice
                checked: settings.showPriceGraph
                text: qsTr("Visa en graf för elpriset")
                onCheckedChanged: {settings.showPriceGraph = checked}
            }
            Slider {
                enabled: settings.loadSpotPrice && settings.showPriceGraph
                handleVisible: settings.loadSpotPrice && settings.showPriceGraph
                id: graph_heigh
                width: parent.width
                minimumValue: 300
                maximumValue: 700
                value: settings.graphHeight
                stepSize: 50
                valueText: value
                label: qsTr("höjd på spotprisgrafen")
                onReleased: {
                    settings.graphHeight = value
                }
            }
            TextField {
                enabled: settings.loadSpotPrice
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                text: settings.transferFee
                label: qsTr("Överföringsavgift (c/kWh)")
                EnterKey.iconSource: "image://theme/icon-m-enter-close"
                EnterKey.onClicked: focus = false
                onTextChanged: {
                    if (text != "")
                        settings.transferFee = text.replace(',','.')
                    else
                        settings.transferFee = 0
                }
            }
            TextField {
                enabled: settings.loadSpotPrice
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                text: settings.electricityTax
                label: qsTr("Elskatt (%)")
                EnterKey.iconSource: "image://theme/icon-m-enter-close"
                EnterKey.onClicked: focus = false
                onTextChanged: {
                    if (text != "")
                        settings.electricityTax = text.replace(',','.')
                    else
                        settings.electricityTax = 0
                }
            }


            SectionHeader {
                text: qsTr("Vind")
            }
            TextSwitch {
                checked: settings.loadWindData
                text: qsTr("Visa vindelsproduktion")
                onCheckedChanged: {settings.loadWindData = checked}
            }
            TextSwitch {
                enabled: settings.loadWindData && !settings.showPriceOnCover
                checked: settings.showWindOnCover
                text: qsTr("Visa vindelsproduktion på coverbilden")
                onCheckedChanged: {settings.showWindOnCover = checked}
            }


            SectionHeader {
                text: qsTr("Autouppdatera värden")
            }
            ComboBox {
                label: "Uppdateringsfrekvens"
                currentIndex: settings.autoRefresh

                menu: ContextMenu {
                    MenuItem { text: qsTr("Av") }
                    MenuItem { text: qsTr("1 minut") }
                    MenuItem { text: qsTr("15 minuter") }
                    MenuItem { text: qsTr("1 timme") }
                    MenuItem { text: qsTr("2 timmar") }
                }
                onCurrentIndexChanged: {
                    settings.autoRefresh = currentIndex

                    switch (currentIndex) {
                    case 1:
                        settings.refreshTimeInMinutes = 1
                        break
                    case 2:
                        settings.refreshTimeInMinutes = 15;
                        break;
                    case 3:
                        settings.refreshTimeInMinutes = 60
                        break
                    case 4:
                        settings.refreshTimeInMinutes = 2*60
                        break
                    }

                    pageStack.find(function(page) { return page.objectName === "FirstPage"; }).update_timer(currentIndex)
                }
            }
        }
    }
}
