import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

CoverBackground {

    ConfigurationGroup {
        id: settings
        path: "/org/vader/solf/settings"
        property bool loadWindData
        property bool showWindOnCover
        property bool loadSpotPrice
        property bool showPriceOnCover
        property int decimals
    }


    Column {
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter
        spacing: (settings.showWindOnCover || settings.showPriceOnCover) ? Theme.paddingMedium : Theme.paddingLarge
        anchors.centerIn: parent



        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.highlightColor
            font.pixelSize: Theme.fontSizeMedium
            text: qsTr("Inomhus")
        }
        Label {
            id: label
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: Theme.fontSizeLarge
            text: pageStack.find(function(page) { return page.objectName === "FirstPage"; }).insideTemp
        }
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.highlightColor
            font.pixelSize: Theme.fontSizeMedium
            text: qsTr("Utomhus")
        }
        Label {
            id: label2
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: Theme.fontSizeLarge
            text: pageStack.find(function(page) { return page.objectName === "FirstPage"; }).outsideTemp
        }
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.highlightColor
            font.pixelSize: Theme.fontSizeMedium
            text: qsTr("Elpris")
            visible: settings.showPriceOnCover && settings.loadSpotPrice
        }
        Label {
            id: label3
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: Theme.fontSizeLarge
            text: pageStack.find(function(page) { return page.objectName === "FirstPage"; }).spotPrice.split('(')[0]
            visible: settings.showPriceOnCover && settings.loadSpotPrice
        }
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.highlightColor
            font.pixelSize: Theme.fontSizeMedium
            text: qsTr("Vindel")
            visible: settings.showWindOnCover && settings.loadWindData
        }
        Label {
            id: label4
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: Theme.fontSizeLarge
            //handleVisible: false
            text: Math.floor(100*pageStack.find(function(page) { return page.objectName === "FirstPage"; })
                             .windValue
                             / pageStack.find(function(page) { return page.objectName === "FirstPage"; })
                             .windMaxValue)
                             + "%"
            visible: settings.showWindOnCover && settings.loadWindData
        }
        Label {
            id: padding
            font.pixelSize: Theme.fontSizeSmall
            text: " "
        }

    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-refresh"
            onTriggered: pageStack.find(function(page) { return page.objectName === "FirstPage"; }).refresh()

        }
    }
}
